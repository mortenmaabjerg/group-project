//Person 1

let p1_visual = function(p) {
    let data;
    let myWidth = p.windowWidth/2-12 //12 margin and borders of the divs
    let myHeight = p.windowHeight/2-34.5 //34,5 due to margin, borders and navbar
    let i = 0;
    let x1 = 2;
    let y1 = myHeight/2.5
    let x2 = 2;
    let y2 = myHeight-myHeight/2.5

    let week = 10

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/group-project/self_tracking/door_exits_p1')
    };

    p.setup = function() {
      p.createCanvas(myWidth, myHeight);
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        while(parseInt(data.exits[i].week) != week){
          p.stroke("black")
          p.strokeWeight(2)
          p.point(x1, myHeight/2)
          week+=1
          x1+=myWidth/(data.exits.length+5);
          x2+=myWidth/(data.exits.length+5);
        }

        p.strokeWeight(1);

        if(data.exits[i].purpose === "Practical"){
          p.stroke("red")
        } else if(data.exits[i].purpose === "Repetition") {
          p.stroke("grey")
        } else if(data.exits[i].purpose === "Leisure"){
          p.stroke("green")
        } else if(data.exits[i].purpose === "Work"){
          p.stroke("blue")
        } else {
          p.stroke("yellow")
        }

      if (data.exits[i].mood === "Positive"){
          y1-=2.4
          y2+=2.4
          p.line(x1, y1, x2, y2)
        } else if (data.exits[i].mood === "Negative"){
          y1+=2.4
          y2-=2.4
          p.line(x1, y1, x2, y2)
        } else if (data.exits[i].mood === "Neutral"){
          p.line(x1, y1, x2, y2)
        }
        i++;
        x1+=myWidth/(data.exits.length+5);
        x2+=myWidth/(data.exits.length+5);
    }
    catch(e){
      i++
    }
  }
}
  var myp5 = new p5(p1_visual, "p1");

//Person 2

let p2_visual = function(p) {
    let data;
    let myWidth = p.windowWidth/2-12 //12 margin and borders of the divs
    let myHeight = p.windowHeight/2-34.5 //34,5 due to margin, borders and navbar
    let i = 0;
    let x1 = 2;
    let y1 = myHeight/2.5
    let x2 = 2;
    let y2 = myHeight-myHeight/2.5

    let week = 10

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/group-project/self_tracking/door_exits_p2')
    };

    p.setup = function() {
      p.createCanvas(myWidth, myHeight);
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        while(parseInt(data.exits[i].week) != week){
          p.stroke("black")
          p.strokeWeight(2)
          p.point(x1, myHeight/2)
          week+=1
          x1+=myWidth/(data.exits.length+5);
          x2+=myWidth/(data.exits.length+5);
        }

        p.strokeWeight(1);

        if(data.exits[i].purpose === "Practical"){
          p.stroke("red")
        } else if(data.exits[i].purpose === "Repetition") {
          p.stroke("grey")
        } else if(data.exits[i].purpose === "Leisure"){
          p.stroke("green")
        } else if(data.exits[i].purpose === "Work"){
          p.stroke("blue")
        } else {
          p.stroke("yellow")
        }

      if (data.exits[i].mood === "Positive"){
          y1-=2.4
          y2+=2.4
          p.line(x1, y1, x2, y2)
        } else if (data.exits[i].mood === "Negative"){
          y1+=2.4
          y2-=2.4
          p.line(x1, y1, x2, y2)
        } else if (data.exits[i].mood === "Neutral"){
          p.line(x1, y1, x2, y2)
        }
        i++;
        x1+=myWidth/(data.exits.length+5);
        x2+=myWidth/(data.exits.length+5);
    }
    catch(e){
      i++
    }
  }
}
  var myp5 = new p5(p2_visual, "p2");

// Person 3

let p3_visual = function(p) {
    let data;
    let myWidth = p.windowWidth/2-12 //12 margin and borders of the divs
    let myHeight = p.windowHeight/2-34.5 //34,5 due to margin, borders and navbar
    let i = 0;
    let x1 = 2;
    let y1 = myHeight/2.5
    let x2 = 2;
    let y2 = myHeight-myHeight/2.5

    let week = 10

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/group-project/self_tracking/door_exits_p3')
    };

    p.setup = function() {
      p.createCanvas(myWidth, myHeight);
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        while(parseInt(data.exits[i].week) != week){
          p.stroke("black")
          p.strokeWeight(2)
          p.point(x1, myHeight/2)
          week+=1
          x1+=myWidth/(data.exits.length+5);
          x2+=myWidth/(data.exits.length+5);
        }

        p.strokeWeight(1);

        if(data.exits[i].purpose === "Practical"){
          p.stroke("red")
        } else if(data.exits[i].purpose === "Repetition") {
          p.stroke("grey")
        } else if(data.exits[i].purpose === "Leisure"){
          p.stroke("green")
        } else if(data.exits[i].purpose === "Work"){
          p.stroke("blue")
        } else {
          p.stroke("yellow")
        }

      if (data.exits[i].mood === "Positive"){
          y1-=2.4
          y2+=2.4
          p.line(x1, y1, x2, y2)
        } else if (data.exits[i].mood === "Negative"){
          y1+=2.4
          y2-=2.4
          p.line(x1, y1, x2, y2)
        } else if (data.exits[i].mood === "Neutral"){
          p.line(x1, y1, x2, y2)
        }
        i++;
        x1+=myWidth/(data.exits.length+5);
        x2+=myWidth/(data.exits.length+5);
    }
    catch(e){
      i++
    }
  }
}
  var myp5 = new p5(p3_visual, "p3");

//Person 4

let p4_visual = function(p) {
    let data;
    let myWidth = p.windowWidth/2-12 //12 margin and borders of the divs
    let myHeight = p.windowHeight/2-34.5 //34,5 due to margin, borders and navbar
    let i = 0;
    let x1 = 2;
    let y1 = myHeight/2.5
    let x2 = 2;
    let y2 = myHeight-myHeight/2.5

    let week = 10

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/group-project/self_tracking/door_exits_p4')
    };

    p.setup = function() {
      p.createCanvas(myWidth, myHeight);
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        while(parseInt(data.exits[i].week) != week){
          p.stroke("black")
          p.strokeWeight(2)
          p.point(x1, myHeight/2)
          week+=1
          x1+=myWidth/(data.exits.length+5);
          x2+=myWidth/(data.exits.length+5);
        }

        p.strokeWeight(1);

        if(data.exits[i].purpose === "Practical"){
          p.stroke("red")
        } else if(data.exits[i].purpose === "Repetition") {
          p.stroke("grey")
        } else if(data.exits[i].purpose === "Leisure"){
          p.stroke("green")
        } else if(data.exits[i].purpose === "Work"){
          p.stroke("blue")
        } else {
          p.stroke("yellow")
        }

      if (data.exits[i].mood === "Positive"){
          y1-=2.4
          y2+=2.4
          p.line(x1, y1, x2, y2)
        } else if (data.exits[i].mood === "Negative"){
          y1+=2.4
          y2-=2.4
          p.line(x1, y1, x2, y2)
        } else if (data.exits[i].mood === "Neutral"){
          p.line(x1, y1, x2, y2)
        }
        i++;
        x1+=myWidth/(data.exits.length+5);
        x2+=myWidth/(data.exits.length+5);
    }
    catch(e){
      i++
    }
  }
}
  var myp5 = new p5(p4_visual, "p4");
