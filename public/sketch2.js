//json_data visualiering

let json_visual = function(p) {
    let data;
    let i = 0;
    //let x = 5;
    //let y = 10;

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/group-project/self_tracking/door_exits_p2')
    };

    p.setup = function() {
      let sketchCanvas = p.createCanvas(1080/2-20, p.windowHeight); //1080 is max width of div elemenet, 20 i the margin og the elements in the div
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        let x = p.random(0, p.width)
        let y = p.random(0, p.height)

        p.stroke("red")
        p.fill("black")
        if(i <= data.exits.length){
          p.text(JSON.stringify(data.exits[i]), x, y)
          //y+=10
          i++
        }
      }
      catch(e){
        i++
      }
    }
  }
  var myp5 = new p5(json_visual, "json_data");
